# This presentation is in plain text and can be read as is.
# You can use https://tools.suckless.org/sent/ to show slides.

PRACTICAL PUBLIC-KEY CRYPTOGRAPHY:
\
An optimal way to repeatedly log onto the clusters
\
\
(C) 2021, Marco van Hulten, CC BY-SA

ABOUT ME
\
⚫ Studied physics
⚫ Research in climatology and ocean modelling
⚫ IT since this year (UiB; Metacenter)
    ▸ scientific computation
    ▸ infrastructure
    ▸ security

HOW TO LOG ONTO LOGIN NODE
\
⚫ We use OpenSSH to log onto the clusters
    ▸ ssh saga.sigma2.no  → login-{1,2,...}.saga.sigma2.no
⚫ Logging in by password is
    ▸ not that convenient
    ▸ not that secure
        ⁃ brute forcing
        ⁃ wrong host

THE SOLUTION
\
Key authentication
    ▸ infeasible to brute force
    ▸ more convenient
\
Two types: ▸ symmetric (shared secret)
                 ▸ public-key cryptography

FUNDAMENTALS
\
Symmetric-key algorithm:
0. Create and communicate a secret key S.
1. Alice encrypts plaintext with S, and sends the ciphertext to Bob.
2. Bob receives and decrypts with S.
3. Both Bob and Alice keep the shared secret key S "secret".

@symmetric.png

Public-key algorithm:
0. Both parties create a keypair (Ap, As), (Bp, Bs) and share Ap and Bp.
1. Alice encrypts plaintext with Bp.
2. Bob receives and decrypts with Bs.
3. Alice keeps her As and Bob his Bs SECRET.

@asymmetric.png

Public-key algorithm advantages (compared
to symmetric key cryptography):
\
    ⚫ No need to share a secret.
    ⚫ Keys may be reused.
\
Important to
    ▸ Protect you secret keys.
    ▸ Share your public keys.

OPENSSH PROTOCOL: OVERVIEW
\
1. When client connects, daemon responds with its public host key.
2. The client compares the key to its own database.
3. Session key established through Diffie–Hellman key agreement.
4. Authentication dialog... (e.g. using public-key cryptography)
5. A session is prepared, including tty, X11 and auth forwarding.
6. Client requests a shell or execution of a command.
Now either side may send data at any time, until client terminates.

OPENSSH PROTOCOL: AUTHENTICATION DIALOG
\
OpenSSH tries these methods (in this order):
    ⁃ host-based authentication
    ⁃ public-key authentication
    ⁃ challenge-response authentication
    ⁃ password authentication

SSH DEMO:
\
PUBLIC-KEY AUTHENTICATION
                 WITH PASSPHRASE
# ssh mhu027@saga.sigma2.no     # password authentication (new and # short)
#   # On https://documentation.sigma2.no/getting_started/create_ssh_keys.html:
#   # Ed25519 key fingerprint is SHA256:ryqAxpKDjNLLa5VeUPclQRaZBOIjd2HFgufUEnn4Jrw
#   # Other ciphers may be added after authentication
# logout                        # ^D
# ssh-keygen -t ed25519 -a 100 -f .ssh/id_sigma2
#                               # enter passphrase to encrypt the key
# cd .ssh/
# ls -la
# cat id_sigma2.pub
# vim config                    # use your regular editor
#   Host saga
#       Hostname saga.sigma2.no
#       User <myusername>
#       IdentityFile .ssh/id_sigma2
# eval $(ssh-agent -t 3600)     # optionally put in ~/.xsession
# ssh-add -L
# ssh-add id_sigma2
# ssh-add -L
# ssh-copy-id -i id_sigma2 saga # password authentication
#                               # saga:.ssh is created if not existing
# ssh saga                      # asks for passphrase if not yet given
# logout                        # ^D
# ssh saga                      # no passphrase asked (until time-out)

BEST PRACTICES
\
⚫ Keep your private key private (security).
⚫ Use a good passphrase (security).
⚫ Use ssh-agent(1) with the right lifetime (security & ease).

ADVANCED TIPS
\
⚫ Forward authentication data: ssh -A
⚫ Set a time-out to ssh-agent: eval $(ssh-agent -t 28800)
⚫ Execute this as a shell command and put it in ~/.xsession.
⚫ Use a login script that adds the identity if not loaded.
⚫ sshd(8), ssh(1), ssh-keygen(1), ssh-agent(1), ssh-config(5)

BIBLIOGRAPHY
\
⚫ https://documentation.sigma2.no/getting_started/create_ssh_keys.html
⚫ sshd(8), ssh(1), ssh-keygen(1), ssh-agent(1), ssh-config(5)

LOGIN SCRIPT (ssha)
\
 #!/usr/bin/env bash
 if [ "$(ssh-add -L | awk '{ print $1 }')" == "ssh-ed25519" ]; then
     echo "Private key $(ssh-add -L | awk '{ print $3 }') was already added."
 else
     ssh-add ~/.ssh/id_sigma2
 fi
 ssh -A "$@"
